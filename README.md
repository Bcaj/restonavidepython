# Projet 6 du parcours Ingénieur IA OpenClassRooms

## Faire une étude de faisabilité
- Analyser la possibilité d'utiliser les données du site YELP
- faire de la NLP sur les avis négatif
- classer les images sans avoir le label.
  - utiliser 2 approches différentes 
  - une avec l'extraction de descripteurs 
  - avec un réseau neuronal convolutif
- Présenter les résultats 

# Commençons par la fin
## Page web
Faites avec DASH, qui permet l'intégration de graphique dynamique fait avec plotly.  
Avantages : serveur sous flask, qui est un framework python, permet une seule codebase pour 
faire tout ce qui est demandé 
- appel API
- lecture de la base de données 
- visualisation de données
- inférence des models

## Données YELP
Lecture du fichier json nécessaire à l'analyse textuelle "review",  
c'est un json de json par ligne, donc possibilité de ne lire qu'une partie lors d'une boucle.  
Ce qui a été fait, et à chaque ligne lu, extraction uniquement des champs souhaités, puis insertion
dans une base de données SQLITE3. Le fichier faisant 5go, la totalité n'est pas nécessaire pour 
cette étude, la boucle c'est arrêté à 200 000 lignes, ce qui est bien plus que suffisant.  
Premier avantage, la bdd local est un fichier de 150mo, ensuite, comme c'est un projet qui n'a pas été
fait en une seule session, il faut recharger en mémoire les données, avec une lecture de la base
cela est un gain de temps énorme. Et finalement l'avantage de pouvoir faire des clauses where lors
de la lecture de données, ce qui est utile pour le projet, en filtrant uniquement sur les 
avis dont les notes est la plus base.

Pour les images, tout a été importé, ce sont les id des photos, ainsi que leurs labels.  
Encore une fois, le SQL va permettre de filtrer en amont plutôt que de tout charger en RAM.

