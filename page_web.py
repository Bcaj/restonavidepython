import sqlite3
import pandas as pd
from dash import Dash, html, dcc
import plotly.express as px
import plotly.figure_factory as ff

conn = sqlite3.connect("data/yelp.db")
df = pd.read_sql("select * from resultats", conn)

# distribution des classes prédites par orb
assignation_cluster = df.pivot_table(index='orb', columns='terrain', values='id', aggfunc='count')
distrib_orb = px.bar(assignation_cluster, barmode='stack', text_auto=True,
                     labels={"value": "Nombre d'Images", "variable": "Catégories Réelles"},
                    )
distrib_orb.update_layout(
    title={'text': "Répartition des Catégories Réelles par Cluster", 'x':0.5}
)

# matrice de couleur pour la matrice de confusion
# dans le notebook, elle est défini algorithmiquement, là j'ai juste c/c :)
colors = [[0.0, 1.0, 1.0, 1.0, 1.0, 0.5],
          [1.0, 0.0, 1.0, 1.0, 1.0, 0.5],
          [1.0, 1.0, 0.0, 1.0, 1.0, 0.5],
          [1.0, 1.0, 1.0, 0.0, 1.0, 0.5],
          [1.0, 1.0, 1.0, 1.0, 0.0, 0.5],
          [0.5, 0.5, 0.5, 0.5, 0.5, 0.5]]

colors_scale = [[0.0, 'green'],
                [0.5, 'blue'],
                [1.0, 'rgb(200,200,200)']]
assignation_cluster['total'] = assignation_cluster.sum(axis=1)
col_sum = assignation_cluster.sum()
assignation_cluster.loc['total'] = col_sum
asso = {0 : 'inside',
        1 : 'menu',
        2 : 'drink',
        3 : 'outside',
        4 : 'food'
        }
assignation_cluster.rename(index=asso, inplace=True)
ordonner = ['inside', 'menu', 'drink', 'outside', 'food', 'total']
assignation_cluster = assignation_cluster[ordonner]
v = assignation_cluster.values
x = ['inside', 'menu', 'drink', 'outside', 'food', 'total']
matrice_orb = ff.create_annotated_heatmap(colors[::-1],
                                  annotation_text=v[::-1],
                                  colorscale=colors_scale,
                                  font_colors=['white'],
                                  x=x,
                                  y=x[::-1]
                                  )
matrice_orb.update_layout(
    title_text="Matrice de confusion",
    title_x=0.5,

    margin=dict(l=150, r=100, t=110, b=100),
)
matrice_orb.add_annotation(dict(
    font=dict(size=16),
    x=0.5,
    y=1.15,
    showarrow=False,
    text="Valeur réelle",
    xref="paper",
    yref="paper"
))
matrice_orb.add_annotation(dict(
    font=dict(size=16),
    x=-0.1,
    y=0.5,
    showarrow=False,
    text="Valeur prédite",
    textangle=-90,
    xref="paper",
    yref="paper"
))

# pour le cnn
cnnr = df.pivot_table(index='cnn_tag', columns='terrain', values='id', aggfunc='count')
cnnr['total'] = cnnr.sum(axis=1)
col_sum = cnnr.sum()
cnnr.loc['total'] = col_sum
cnnr.fillna(0, inplace=True)
cnnr = cnnr.astype(int)
vc = cnnr.values
xx = ['drink', 'food', 'inside', 'menu', 'outside', 'total']
matrice_cnn = ff.create_annotated_heatmap(colors[::-1],
                                  annotation_text=vc[::-1],
                                  colorscale=colors_scale,
                                  font_colors=['white'],
                                  x=xx,
                                  y=xx[::-1]
                                  )
matrice_cnn.update_layout(
    title_text="Matrice de confusion",
    title_x=0.5,

    margin=dict(l=150, r=100, t=110, b=100),
)
matrice_cnn.add_annotation(dict(
    font=dict(size=16),
    x=0.5,
    y=1.15,
    showarrow=False,
    text="Valeur réelle",
    xref="paper",
    yref="paper"
))
matrice_cnn.add_annotation(dict(
    font=dict(size=16),
    x=-0.1,
    y=0.5,
    showarrow=False,
    text="Valeur prédite",
    textangle=-90,
    xref="paper",
    yref="paper"
))

app = Dash(__name__)

app.layout = html.Div([
    html.H1(children='Avis Restau',
            style={'textAlign': 'center',
                   'fontsize': 'XL'}),
    html.H2(children='Analyse textuel'),
    html.Img(src="assets/nuage.png",
             style={'margin': '0 auto', 'display': 'block'}),
    html.Br(),
    html.P(children='Visualisation en 2 dimensions de la répartition des sujets et des mots,'
                      ' après analyse textuel des commentaires négatif',
             ),
    html.Br(),
    html.Iframe(
        src='/assets/lda_vis.html',
        width="1500",
        height="888",
        style={"border": "none", 'margin': '0 auto', "display": 'block'}
    ),
    html.H2(children="Reconnaissance d'image"),
    html.P(children="Pour cette étude de faisabilité 2 approches seront étudié, "
                      "la première par algorithme de vision par ordinateur utilisant ORB. "
                      "Et deuxièmement, une méthode par apprentissage machine d'un réseau"
                      " neuronal convolutif (CNN en anglais)"),
    html.H3(children="ORB"),
    html.P(children="Approche par analyse des descripteurs de toutes les images, "
                      "puis clusters de ses points clé des images pour former des catégories."),

    html.P(children="Suite à l'extraction des points clé d'une image, on obtient un vecteur de N dimensions "
                    "ou N est le nombre de catégories différentes que l'on souhaite obtenir, et la valeur "
                    "est le poids que les caractéristiques sont de chaque catégories. "
                    "On réduit la dimension en 2 pour l'afficher sur un plan en 2D, ou la couleur est le "
                    "label du groupe prédit"),
    html.Img(src="assets/tsne.png",
             style={'margin': '0 auto', 'display': 'block'}),

    html.P(children="Après le choix de la classe suite a l'algo ORB, une seule catégorie est prédite, "
                    "en dessous on voit la différente entre les groupes numéroté, qui n'ont pas encore "
                    "le 'vrai' label car on ne le connait pas suite à un Kmeans. "
                    "On peut voir la répartition entre la vérité terrain des images, et où elles se "
                    "retrouvent après cette suite d'algorithme."),

    dcc.Graph(id='distrib_orb', figure=distrib_orb),

    html.P(children="Après association des labels réel et les classes ORB restantes, en faissant "
                    "par éliminations des classes qui comportent le plus d'une valeur réel. "
                    "On peut faire une matrice de confusion multiclasse"),
    dcc.Graph(id="matrice_orb", figure=matrice_orb),
    html.P(children=["Après calcul du score d'éxactitude : toutes les bonnes prédicitons / nombre total"
                    " et du score f1 : qui est la moyenne entre la précision et le rappel "
                    "on obtient ",
                     html.B("~0.32"),
                     " pour les deux, le calcul donne un score visiblement différent quand les données"
                     " sont moins bien répartie"]),

    html.H3(children="CNN"),
    html.P(children="Avec un réseau de neurones pré-entrainé, le ResNet18, seulement la dernière couche "
                    "a été surentrainné pour ne sortir que 5 catégories lors des prédictions. "
                    "Le jeu d'entrainement contenait 1000 images, avec 10 epochs."),

    dcc.Graph(id="matrice_cnn", figure=matrice_cnn),
    html.P(children=["Pour les scores d'éxactitude et f1 : ont est cette fois sur",
                     html.B(" ~0.92")]),


    # html.H3(children="Conclusion analyse images"),
    # html.P(children="Échantillon d'images avec applicaiton des différentes méthodes"),
    # html.Div([
    #     html.P(children="ORB", style={'width': '15%', 'display': 'inline-block'}),
    #     html.P(children="CNN", style={'width': '85%', 'display': 'inline-block'}),
    #     html.Div([
    #         html.Img(src="assets/orbtest.png",style={'width': '40%', 'display': 'inline-block'}),
    #         html.Div(style={'width': '10px', 'height': '800px', 'background-color': 'black', 'display': 'inline-block'}),
    #         html.Img(src="assets/cnn_test.png",style={'width': '40%', 'display': 'inline-block'} )
    #         ])
    #     ]),

    html.H3(children="Conclusion analyse images"),
    html.P(children="Échantillon d'images avec application des différentes méthodes"),
    html.Div([
        html.Div([
            html.P(children="ORB", style={'textAlign': 'center'}),
        ], style={'width': '50%', 'display': 'inline-flex', 'justifyContent': 'center', 'alignItems': 'center'}),

        #html.Div(style={'width': '10px', 'height': '800px', 'backgroundColor': 'black', 'display': 'inline-block'}),

        html.Div([
            html.P(children="CNN", style={'textAlign': 'center'}),
        ], style={'width': '50%', 'display': 'inline-flex', 'justifyContent': 'center', 'alignItems': 'center'}),
    ], style={'display': 'flex'}),

    html.Div([
        html.Img(src="assets/orbtest.png", style={'width': '40%', 'display': 'inline-block'}),
        html.Img(src="assets/cnn_test.png", style={'width': '40%', 'display': 'inline-block'})
    ], style={'display': 'flex', 'justifyContent': 'space-between'}),

    html.Br(),

    html.P(children=["On constate que l'utilisation de la méthode ORB fonctionne occasionnellement, en cas "
                    "d'image qui comprend autant d'information d'une catégorie qu'une autre pour l'ordinateur "
                    "alors le label sera faussé. "
                    "Alors qu'avec un CNN la précision du niveau d'un être humain qui analyserai une image. "
                    "J'entends par cette phrase, que l'interprétation des labels de certaines images est ressorti. "
                    "C'est pour celà que cette étude démontre qu'il faut choisir un ",
                     html.B("réseau neuronal convolutif pré-entrainé et ajouter la dernière couche "
                            "avec le jeu de donnée a disposition pour la tache prévu.")])
])


if __name__ == '__main__':
    app.run(debug=False)