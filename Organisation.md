# Télécharger le jeu de donnée
Nécessite de donner une adresse mail et de valider l'accord du site <br>
https://www.yelp.com/dataset/download <br>
## json 
comprend les dataset textuel 
  - business
  - checkin
  - review
  - tip
  - user

Pour la demande d'analyse présente, il n'y a que le fichier review qui va être utilisé, <br>
Sur la doc de yelp concernant les dataset https://www.yelp.com/dataset/documentation/main <br> 
on peut y lire  
```
*review.json*
Contains full review text data including the user_id that wrote the review and the business_id the review is written for.
{
    // string, 22 character unique review id
    "review_id": "zdSx_SD6obEhz9VrW9uAWA",

    // string, 22 character unique user id, maps to the user in user.json
    "user_id": "Ha3iJu77CxlrFm-vQRs_8g",

    // string, 22 character business id, maps to business in business.json
    "business_id": "tnhfDv5Il8EaGSXZGiuQGg",

    // integer, star rating
    "stars": 4,

    // string, date formatted YYYY-MM-DD
    "date": "2016-03-09",

    // string, the review itself
    "text": "Great place to hang out after work: the prices are decent, and the ambience is fun. It's a bit loud, but very lively. The staff is friendly, and the food is good. They have a good selection of drinks.",

    // integer, number of useful votes received
    "useful": 0,

    // integer, number of funny votes received
    "funny": 0,

    // integer, number of cool votes received
    "cool": 0
}
```

On y trouve 3 informations qui seront utile, le reste ne concerne pas notre demande
1. l'id du commentaire : permet l'unicité et ne pas obtenir de doublons
2. l'id du restaurant : qui peut relier la photo qui cite le lieu
3. le contenue textuel du commentaire
4. start : qui est la note, dans notre traitement, ils ne faut garder que ceux a 1

## Photos
Contient 200 000 photos en format jpg <br>
Un fichier json qui contient : 
```
photo.json

Contains photo data including the caption and classification (one of "food", "drink", "menu", "inside" or "outside").

{
    // string, 22 character unique photo id
    "photo_id": "_nN_DhLXkfwEkwPNxne9hw",
    // string, 22 character business id, maps to business in business.json
    "business_id" : "tnhfDv5Il8EaGSXZGiuQGg",
    // string, the photo caption, if any
    "caption" : "carne asada fries",
    // string, the category the photo belongs to, if any
    "label" : "food"
}
```
Toutes les informations seront utiles

## Transformation des json en sql
Ne pas avoir a tout recharger en mémoire du json, transformer en dataframe pour filtrer.  
Une fois la table créée, une requete sur where star = 1 et le dataframe sera créé directement.
Un échantillon de 200 000 ligne pour vérifier si il y a bien suffisamment d'avis à 1
```python
import json
import pandas as pd
import sqlite3

conn = sqlite3.connect('data/yelp.db')
i = 0
with open('data/dataset/yelp_academic_dataset_review.json', 'r', encoding='utf-8') as fichier:
    for ligne in fichier:
        data = json.loads(ligne)
        datas = {'review_id': data['review_id'],
                 'user_id': data['user_id'],
                 'business_id': data['business_id'],
                 'stars' : data['stars'],
                 'text': data['text']
                 }
        df = pd.DataFrame([datas])
        df.to_sql('review', con=conn, if_exists='append', index=False)

        i+=1
        if i == 200_000:
            break
```

